# from pyexpat import model
from django import forms
from .models import Entries

# class ProductForm(forms.ModelForm):
#     class Meta:
#         model = Product
#         fields = ['name', 'category', 'quantity']
# class OrderForm(forms.ModelForm):
#     class Meta:
#         model = Order
#         fields = ['product','order_quantity']

class EntryForm(forms.ModelForm):
    class Meta:
        model = Entries
        fields = ['subject','department','remark','section']

class ApprovalForm(forms.ModelForm):
   class Meta:
        model = Entries
        fields = ['status','is_rejected']

class RejectForm(forms.ModelForm):
       class Meta:
        model = Entries
        fields = ['is_rejected']