from django.urls import path
from . import views

urlpatterns = [
    path('dashboard/',views.entry, name='dashboard-entry'),
    path('dashboard/add_entry',views.add_entry, name='dashboard-entry-add_entry'),
    path('dashboard/approved/<int:pk>',views.staff_index, name='dashboard-entry-approved'),
    # path('dashboard/rejected/<int:pk>',views.staff_index, name='dashboard-entry-rejected'),
    path('staff/',views.staff,name='dashboard-staff'),
    path('staff/detail/<int:pk>',views.staff_detail,name='dashboard-staff-detail'),
    # path('product/',views.product,name='dashboard-product'),
    # path('product/delete/<int:pk>',views.product_delete,name='dashboard-product-delete'),
    # path('product/edit/<int:pk>',views.product_edit,name='dashboard-product-edit'),
    # path('order/',views.order,name='dashboard-order'),
    path('report_pdf/',views.report_pdf,name='report_pdf'),
]