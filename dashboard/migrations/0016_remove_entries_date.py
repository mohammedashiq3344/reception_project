# Generated by Django 4.1 on 2022-09-09 13:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0015_entries_date_alter_entries_department'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entries',
            name='date',
        ),
    ]
