from django.db import models
# from dashboard.views import product
from django.contrib.auth.models import User
from datetime import datetime


# Create your models here.
# CATEGORY = (
#     ('Stationary','Stationary'),
#     ('Electronics','Electronics'),
#     ('Food','Food'),
#     ('Kitchenware','Kitchenware'),
#     ('Furniture','Furniture'),   
# )

DEPARTMENT = (
    ('Architrcture','Architrcture'),
    ('Civil Engineering','Civil Engineering'),
    ('CSE','CSE'),
    ('Electronics&communication','Electronics&communication'),
    ('Electrical&Electronics','Electrical&Electronics'),
    ('Information Technology','Information Technology'),
    ('Mechanical','Mechanical'),
    ('MCA','MCA'),
)

SECTION = (
    ('Principal','Principal'),
    ('Academic','Academic'),
    ('Dicipline','Dicipline'), 
    ('Library','Library'), 
    ('Scholarship','Scholarship'), 
    ('Superintendent','Superintendent'),
    ('Accounts','Accounts'),    
    ('CNC','CNC'), 
   
)

# class Product(models.Model):
#     name = models.CharField(max_length=100,null=True)
#     category = models.CharField(max_length=20,choices=CATEGORY,null=True)
#     quantity = models.PositiveIntegerField(null=True)

#     def __str__(self):
#         return f'{self.name}-{self.quantity}'

class Entries(models.Model):
    subject = models.CharField(max_length=100,null=True)
    department = models.CharField(max_length=200,choices=DEPARTMENT, null=True)
    remark = models.CharField(max_length=100,null=True)
    section = models.CharField(max_length=20,choices=SECTION,null=True)
    status = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    staff = models.ForeignKey(User, models.CASCADE,null=True)
    # date = models.DateTimeField(auto_now_add=True)


    

    def __str__(self):
        return f'{self.subject}-{self.department}-{self.remark}-{self.section}-{self.status}-{self.is_rejected}-{self.staff}'

        
# class Order(models.Model):
#     product = models.ForeignKey(Product,on_delete=models.CASCADE,null=True)
#     staff = models.ForeignKey(User, models.CASCADE)
#     order_quantity = models.PositiveIntegerField(null=True)
#     date = models.DateTimeField(auto_now_add=True)

#     class Meta:
#         verbose_name_plural = 'Order'

#     def __str__(self):
#         return f'{self.product}order by{self.staff.username}-{self.order_quantity}-{self.date}'

# class Staff(models.Model):
#     Product