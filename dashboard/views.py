# from multiprocessing import context
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .models import Entries
# from .models import Order
from django.contrib.auth.models import User
from .forms import EntryForm,ApprovalForm
from django.contrib import messages
from django.db.models import Q
# from django.shortcuts import get_object_or_404

# xtml2pdf
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa



# Create your views here.

# @login_required
# def index(request):
#     orders = Order.objects.all()
#     products = Product.objects.all()
#     # staffs = User.objects.all()
#     order_count = orders.count()
#     product_count = products.count()
#     workers_count =  User.objects.all().count()
#     if request.method == 'POST':
#         form = OrderForm(request.POST)
#         if form.is_valid():
#             instance = form.save(commit=False)
#             instance.staff = request.user
#             instance.save()
#             order_request = form.cleaned_data.get('product')
#             messages.success(request, f'{order_request} has been created successfully')
#             redirect('dashboard/index')
#     else:
#         form = OrderForm()
#     context = {
#         'orders': orders,
#         'form' : form,
#         'products' : products,
#         'order_count' : order_count,
#         'product_count' : product_count,
#         'workers_count'  : workers_count,
#     }
#     return render(request, 'dashboard/index.html',context)


@login_required
def entry(request):
    # current_user = request.user
    # requests = Entries.objects.all()
    # entries = Entries.objects.filter(section = current_user)
    if 'q' in request.GET:
        q = request.GET['q']
        entries = Entries.objects.filter(Q(subject__icontains=q) | Q(section__icontains=q) | Q(department__icontains=q))
    else:
        entries = Entries.objects.all()
    entries_count = entries.count()
    pending_count = Entries.objects.filter( Q(status = False) & Q( is_rejected = False) ).count()
    rejected_count = Entries.objects.filter( Q(status = False) & Q( is_rejected = True) ).count()
    approved_count = Entries.objects.filter( Q(status = True) & Q( is_rejected = False) ).count()


    context = {
        
        'entries': entries,
        'entries_count': entries_count,
        'pending_count' : pending_count,
        'rejected_count' : rejected_count,
        'approved_count' : approved_count
    }
    return render(request, 'dashboard/entry.html', context)

@login_required
def staff_index(request, pk):
    obj = Entries.objects.filter(pk=pk)
    obj = Entries.objects.get(pk=pk)
    # if request.method == 'POST':
    #     form = ApprovalForm(request.POST)
    #     if form.is_valid():
    #         obj.status = True
    #         # obj.is_rejected = True
    #         obj.save()
    #         # form.save()
    #         return redirect('dashboard-entry')
    if 'approve' in request.POST:
        form = ApprovalForm(request.POST)
        if form.is_valid():
            obj.status = True
            obj.save()
            # form.save()
            return redirect('dashboard-entry')
    elif 'reject' in request.POST:
        form = ApprovalForm(request.POST)
        if form.is_valid():
            obj.is_rejected = True
            obj.save()
            # form.save()
            return redirect('dashboard-entry')
    else:
        form = ApprovalForm()
    context = {
        'obj' : obj,
        'form': form
    }
    return render(request, 'dashboard/staff_index.html', context)

@login_required
def add_entry(request):
    if request.method == 'POST':
        form = EntryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('dashboard-entry')

    else:
        form = EntryForm()

    context={
        'form' : form,
    }
    return render(request, 'dashboard/add_entry.html', context)

@login_required
def staff(request):
    workers = User.objects.all()
    # workers_count = workers.count()
    # product_count = Product.objects.all().count()
    # order_count = Order.objects.all().count()

    context = {
        'workers': workers,
        # 'workers_count': workers_count,
        # 'product_count' : product_count,
        # 'order_count' : order_count,
    }
    return render(request, 'dashboard/staff.html', context)

@login_required
def staff_detail(request, pk):
    workers = User.objects.get(id=pk)
    # order_count = Order.objects.all().count()
    # product_count = Product.objects.all().count()
    # workers_count =  User.objects.all().count()
    context = {
        'workers': workers,
        # 'order_count' : order_count,
        # 'product_count' : product_count,
        # 'workers_count'  : workers_count,
    }
    return render(request, 'dashboard/staff_detail.html',context)



# @login_required
# def product(request):
#     items = Product.objects.all()
    # product_count = items.count()
    # order_count = Order.objects.all().count()
    # items = Product.objects.raw('SELECT * FROM dashboard_product')
    # workers_count = User.objects.all().count()

    # workers_count = workers.count()
    # if request.method == 'POST':
    #     form = ProductForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #         product_name = form.cleaned_data.get('name')
    #         messages.success(request, f'{product_name} has been added')
    #         redirect('dashboard/product')
    # else:
    #     form = ProductForm()
    # context = {
        # 'items': items,
        # 'form': form,
        # 'product_count' : product_count,
        # 'workers_count' : workers_count,
        # 'order_count' : order_count,

    # }
    # return render(request, 'dashboard/product.html',context)

# @login_required
# def product_delete(request, pk):
#     items = Product.objects.get(id=pk)
#     if request.method == 'POST':
#         items.delete()
#         return redirect('dashboard-product')
#     return render(request, 'dashboard/product_delete.html')

# @login_required
# def product_edit(request, pk):
#     items = Product.objects.get(id=pk)
#     if request.method == 'POST':
#         form = ProductForm(request.POST, instance=items)
#         if form.is_valid():
#             form.save()
#             return redirect('dashboard-product')
#     else:
#         form = ProductForm(instance=items)
#     context={
#         'form' : form,
#     }
#     return render(request, 'dashboard/product_edit.html',context)


# @login_required
# def order(request):
#     orders = Order.objects.all()
    # order_count = orders.count()
    # workers_count = User.objects.all().count()
    # product_count = Product.objects.all().count()


    # context = {
    #     'orders' : orders,
        # 'workers_count' : workers_count,
        # 'order_count' : order_count,
        # 'product_count' : product_count,

    # }
    # return render(request, 'dashboard/order.html' ,context)
@login_required
def report_pdf(request):
    entries = Entries.objects.all()
    template_path = 'dashboard/entry_pdf.html'
    context = {'entries': entries}
    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename="report.pdf"'
    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)

    # create a pdf
    pisa_status = pisa.CreatePDF(
       html, dest=response)
    # if error then show some funny view
    if pisa_status.err:
       return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response
