from django.contrib import admin
from .models import Entries

admin.site.site_header = "Reception"

# class ProductAdmin(admin.ModelAdmin):
#     list_display = ('name','category', 'quantity')
#     list_filter = ['category']

# class OrderAdmin(admin.ModelAdmin):
#     list_display = ('product','staff', 'order_quantity', 'date')
#     # list_filter = ['category']

class EntriesAdmin(admin.ModelAdmin):
    list_display = ('id','subject','department', 'remark', 'section','status','is_rejected','staff')
    list_filter = ['section']
# Register your models here.
# admin.site.register(Product, ProductAdmin)
# admin.site.register(Order, OrderAdmin)
admin.site.register(Entries, EntriesAdmin)